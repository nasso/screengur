package io.github.nasso.imgurapi;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.function.Consumer;

import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class ImgurAPI {
	private static final String API_URL = "https://api.imgur.com/3";
	
	private static Gson gson = new Gson();
	
	public static ImgurImage uploadImage(BufferedImage img, Consumer<Float> onprogress, Runnable onwaiting) throws IOException {
		final String lineEnd = "\r\n";
		final String twoHyphens = "--";
		final String boundary = "*****";
		final String contentDispStr = "Content-Disposition: form-data; name=\"image\"";
		final int lineEndB = lineEnd.getBytes().length;
		final int twoHyphensB = twoHyphens.getBytes().length;
		final int boundaryB = boundary.getBytes().length;
		final int contentDispStrB = contentDispStr.getBytes().length;
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(img, "png", baos);
		ByteArrayInputStream imgin = new ByteArrayInputStream(baos.toByteArray());
		
		long bytesAvailable = imgin.available();
		int maxBufferSize = 1024;
		
		long totalBytes = bytesAvailable;
		
		long requestLength = totalBytes + twoHyphensB * 3 + boundaryB * 2 + lineEndB * 5 + contentDispStrB;
		
		URL url = new URL(API_URL + "/image");
		
		HttpsURLConnection c = (HttpsURLConnection) url.openConnection();
		c.setRequestMethod("POST");
		c.setDoInput(true);
		c.setDoOutput(true);
		c.setRequestProperty("Connection", "close");
		c.setFixedLengthStreamingMode(requestLength);
		c.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
		oauth2(c);
		c.connect();
		
		DataOutputStream dos = new DataOutputStream(c.getOutputStream());
		dos.writeBytes(twoHyphens + boundary + lineEnd);
		dos.writeBytes(contentDispStr + lineEnd);
		dos.writeBytes(lineEnd);
		
		byte[] buffer = new byte[maxBufferSize];
		
		int bytesRead = 0;
		long totalReaded = 0;
		
		while((bytesRead = imgin.read(buffer, 0, maxBufferSize)) > 0) {
			dos.write(buffer, 0, bytesRead);
			totalReaded += bytesRead;
			
			onprogress.accept((float) totalReaded / (float) totalBytes);
			
			bytesAvailable = imgin.available();
		}
		
		onwaiting.run();
		
		dos.writeBytes(lineEnd);
		dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
		
		imgin.close();
		dos.flush();
		dos.close();
		
		StringBuilder response = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
		String line;
		while((line = reader.readLine()) != null) {
			response.append(line);
		}
		
		/*
		System.out.println("X-RateLimit-UserLimit: " + c.getHeaderField("X-RateLimit-UserLimit"));
		System.out.println("X-RateLimit-UserRemaining: " + c.getHeaderField("X-RateLimit-UserRemaining"));
		System.out.println("X-RateLimit-UserReset: " + c.getHeaderField("X-RateLimit-UserReset"));
		System.out.println("X-RateLimit-ClientLimit: " + c.getHeaderField("X-RateLimit-ClientLimit"));
		System.out.println("X-RateLimit-ClientRemaining: " + c.getHeaderField("X-RateLimit-ClientRemaining"));
		System.out.println();
		System.out.println("X-Post-Rate-Limit-Limit: " + c.getHeaderField("X-Post-Rate-Limit-Limit"));
		System.out.println("X-Post-Rate-Limit-Remaining: " + c.getHeaderField("X-Post-Rate-Limit-Remaining"));
		System.out.println("X-Post-Rate-Limit-Reset: " + c.getHeaderField("X-Post-Rate-Limit-Reset"));
		System.out.println();
		*/
		
		JsonObject json = gson.fromJson(response.toString(), JsonObject.class);
		if(isSuccess(json)) { 
			return gson.fromJson(json.get("data"), ImgurImage.class);
		}
		
		return null;
	}
	
	public static BufferedImage getBufferedImage(ImgurImage img) throws IOException {
		return ImageIO.read(new URL(img.getLink()));
	}
	
	private static void oauth2(HttpsURLConnection c) {
		// to get a client-id (free, and instantly): https://api.imgur.com/oauth2/addclient
		c.setRequestProperty("Authorization", "Client-ID f89d62e33a6d4aa");
	}
	
	private static boolean isSuccess(JsonObject json) {
		if(json == null) {
			return false;
		}
		
		boolean succeed = (json.get("success").getAsBoolean() && json.get("status").getAsInt() == 200);
		
		if(!succeed) {
			if(json.get("status").getAsInt() == 429){
				System.err.println("Out of credits!! (a request returned code 426)");
			}
		}
		
		return succeed;
	}
}
