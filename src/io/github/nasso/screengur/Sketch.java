package io.github.nasso.screengur;

import java.awt.Color;
import java.awt.Graphics2D;

public abstract class Sketch {
	protected float toolSize = 0;
	protected Color color;
	
	public Sketch(float toolSize, Color toolColor) {
		this.setToolSize(toolSize);
		this.setToolColor(toolColor);
	}
	
	public void setToolSize(float v) {
		this.toolSize = v;
	}
	
	public float getToolSize() {
		return this.toolSize;
	}
	
	public abstract void draw(Graphics2D g2d);
	public abstract Sketch clone();

	public Color getToolColor() {
		return color;
	}

	public void setToolColor(Color toolColor) {
		this.color = new Color(toolColor.getRed(), toolColor.getGreen(), toolColor.getBlue(), toolColor.getAlpha());
	}
}
