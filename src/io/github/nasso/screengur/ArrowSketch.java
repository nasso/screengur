package io.github.nasso.screengur;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;

public class ArrowSketch extends StrokedSketch {
	private int sx, sy, ex, ey;
	
	public ArrowSketch(int startx, int starty, int endx, int endy, float toolSize, Color toolColor) {
		super(toolSize, toolColor);
		
		sx = startx;
		sy = starty;
		ex = endx;
		ey = endy;
	}
	
	public void setStartX(int v) {
		sx = v;
	}
	
	public void setStartY(int v) {
		sy = v;
	}
	
	public void setEndX(int v) {
		ex = v;
	}
	
	public void setEndY(int v) {
		ey = v;
	}
	
	public void setValues(int sx, int sy, int ex, int ey) {
		this.sx = sx;
		this.sy = sy;
		this.ex = ex;
		this.ey = ey;
	}
	
	public void draw(Graphics2D g2d) {
		int d = (int) new Point(sx, sy).distance(new Point(ex, ey));

		g2d.setStroke(this.stroke);
		g2d.setColor(this.color);
		
		AffineTransform backup = g2d.getTransform();
		g2d.translate(sx, sy);
		g2d.rotate(Math.atan2(ey - sy, ex - sx));
		g2d.drawLine(0, 0, d, 0);
		g2d.drawLine(d, 0, (int) (d - 2 * toolSize), (int) (2 * toolSize));
		g2d.drawLine(d, 0, (int) (d - 2 * toolSize), (int) (-2 * toolSize));
		g2d.setTransform(backup);
	}

	public Sketch clone() {
		return new ArrowSketch(sx, sy, ex, ey, toolSize, color);
	}
}
