package io.github.nasso.screengur;

public enum Tool {
	NONE,
	PEN,
	TEXT,
	LINE,
	ARROW,
	RECTANGLE
}
