package io.github.nasso.screengur;

import io.github.nasso.utils.Utils;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class RectangleSketch extends StrokedSketch {
	private int sx, sy, ex, ey;
	
	private Rectangle rect = new Rectangle();
	
	public RectangleSketch(int startx, int starty, int endx, int endy, float toolSize, Color toolColor) {
		super(toolSize, toolColor);
		
		sx = startx;
		sy = starty;
		ex = endx;
		ey = endy;
	}
	
	public void setStartX(int v) {
		sx = v;
	}
	
	public void setStartY(int v) {
		sy = v;
	}
	
	public void setEndX(int v) {
		ex = v;
	}
	
	public void setEndY(int v) {
		ey = v;
	}
	
	public void setValues(int sx, int sy, int ex, int ey) {
		this.sx = sx;
		this.sy = sy;
		this.ex = ex;
		this.ey = ey;
	}
	
	public void draw(Graphics2D g2d) {
		g2d.setStroke(this.stroke);
		g2d.setColor(this.color);
		
		Utils.constructRectangle(sx, sy, ex, ey, this.rect);
		g2d.drawRect(this.rect.x, this.rect.y, this.rect.width, this.rect.height);
	}

	public Sketch clone() {
		return new RectangleSketch(sx, sy, ex, ey, toolSize, color);
	}
}
