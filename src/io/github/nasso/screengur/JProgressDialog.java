package io.github.nasso.screengur;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

public class JProgressDialog extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private static final String CARD_BAR = "bar";
	private static final String CARD_FLD = "field";
	
	private JProgressBar bar = new JProgressBar(0, 100);
	private JButton btnCopy = new JButton("Copy");
	private JTextField textField = new JTextField();
	
	private CardLayout layout = new CardLayout(8, 8);
	
	private JPanel masterContainer = new JPanel();
	private JPanel containerBar = new JPanel();
	private JPanel containerField = new JPanel();
	
	public JProgressDialog() {
		btnCopy.setPreferredSize(new Dimension(70, btnCopy.getHeight()));
		btnCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(textField.getText()), null);
			}
		});
		
		containerBar.setLayout(new BorderLayout());
		containerBar.add(bar, BorderLayout.CENTER);
		
		containerField.setLayout(new BorderLayout(8, 8));
		containerField.add(textField, BorderLayout.CENTER);
		containerField.add(btnCopy, BorderLayout.WEST);
		
		masterContainer.setLayout(layout);
		masterContainer.add(containerBar, CARD_BAR);
		masterContainer.add(containerField, CARD_FLD);
		
		layout.show(masterContainer, CARD_BAR);
		
		this.setContentPane(masterContainer);
		this.setSize(250, 80);
	}
	
	public void setPercent(int percent) {
		this.bar.setValue(percent);
	}
	
	public void setLoading(){
		this.bar.setIndeterminate(true);
	}
	
	public void showLink(String link) {
		this.textField.setText(link);
		this.layout.show(masterContainer, CARD_FLD);
	}
}
