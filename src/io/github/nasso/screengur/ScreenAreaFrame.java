package io.github.nasso.screengur;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JDialog;

public class ScreenAreaFrame extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private ScreenAreaContainer container;
	
	private Robot bot;
	
	private Point pointA = new Point();
	private Point pointB = new Point();
	
	public ScreenAreaFrame(int width, int height) {
		try {
			this.bot = new Robot();
		} catch (AWTException e2) {
			e2.printStackTrace();
		}
		
		this.setBounds(0, 0, width, height);
		this.setUndecorated(true);
		this.setBackground(new Color(0f, 0f, 0f, 0f));
		this.setAlwaysOnTop(true);
		
		this.container = new ScreenAreaContainer();
		this.container.setArea(0, 0, 0, 0);
		this.container.setOnFinished(() -> {
			this.container.setArea(0, 0, 0, 0);
			setVisible(false);
		});
		
		this.setContentPane(container);
		
		this.addMouseListener(new MouseListener() {
			public void mouseReleased(MouseEvent e) {
				pointB.setLocation(e.getLocationOnScreen());
			}
			
			public void mousePressed(MouseEvent e) {
				pointA.setLocation(e.getLocationOnScreen());
			}
			
			public void mouseExited(MouseEvent e) {
				
			}
			
			public void mouseEntered(MouseEvent e) {
				
			}
			
			public void mouseClicked(MouseEvent e) {
				
			}
		});
		
		this.addMouseMotionListener(new MouseMotionListener() {
			public void mouseMoved(MouseEvent e) {
				
			}
			
			public void mouseDragged(MouseEvent e) {
				setArea(pointA.x, pointA.y, e.getXOnScreen(), e.getYOnScreen());
			}
		});
	}
	
	public void setArea(int ax, int ay, int bx, int by) {
		container.setArea(ax, ay, bx, by);
	}
	
	public void takeScreenshot() {
		this.container.setScreen(bot.createScreenCapture(new Rectangle(0, 0, this.getWidth(), this.getHeight())));
		this.setVisible(true);
	}
}
