package io.github.nasso.screengur;

import io.github.nasso.imgurapi.ImgurAPI;
import io.github.nasso.imgurapi.ImgurImage;
import io.github.nasso.utils.Utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.filechooser.FileFilter;

public class ScreenAreaContainer extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	private static final Color OFF_COLOR = new Color(0f, 0f, 0f, 0.6f);
	
	private static final String[] TOOLS_NAMES = {"Pen", "Text", "Line", "Arrow", "Rectangle"};
	private static final Tool[] TOOLS_VALUES = {Tool.PEN, Tool.TEXT, Tool.LINE, Tool.ARROW, Tool.RECTANGLE};

	private ScreenAreaInternalFrame internalFrame = new ScreenAreaInternalFrame();
	
	private JFileChooser fileChooser;
	private String[] formatsImage = {"PNG", "JPG/JPEG", "BMP"};
	
	private BufferedImage screen;
	private Runnable onfinished;
	
	private JPanel btnPanel = new JPanel();
	private JButton btnSend = new JButton("Send to Imgur");
	private JButton btnCopy = new JButton("Copy");
	private JButton btnSave = new JButton("Save");
	private JLabel lblSize = new JLabel("0w0 what's this");
	private JToggleButton[] btnTools = new JToggleButton[TOOLS_NAMES.length];
	private ButtonGroup toolsGroup = new ButtonGroup();
	private JButton btnColor = new JButton();
	
	public ScreenAreaContainer() {
		this.fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileFilter() {
			public boolean accept(File f) {
				String name = f.getName().toLowerCase();
				return name.endsWith(".png") || name.endsWith(".jpg") || name.endsWith(".jpeg") || name.endsWith(".bmp");
			}

			public String getDescription() {
				return "image files (.png, .jpg, .jpeg, .bmp)";
			}
		});
		
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
			public boolean dispatchKeyEvent(KeyEvent e) {
				if(e.getID() == KeyEvent.KEY_PRESSED) {
					if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
						onfinished();
					}
					
					if(e.getModifiersEx() == KeyEvent.CTRL_DOWN_MASK) {
						if(e.getKeyCode() == KeyEvent.VK_Z) {
							internalFrame.actionUndo();
						} else if(e.getKeyCode() == KeyEvent.VK_Y) {
							internalFrame.actionRedo();
						}
					}
				}
				
				return false;
			}
		});
		
		btnSend.addActionListener(this);
		btnCopy.addActionListener(this);
		btnSave.addActionListener(this);
		btnColor.addActionListener(this);
		
		btnSend.setFocusable(false);
		btnCopy.setFocusable(false);
		btnSave.setFocusable(false);
		btnColor.setFocusable(false);
		
		btnColor.setBackground(this.internalFrame.getToolColor());
		
		for(int i = 0; i < btnTools.length; i++) {
			btnTools[i] = new JToggleButton(TOOLS_NAMES[i]);
			btnTools[i].setActionCommand("Select");
			btnTools[i].addActionListener(this);
			btnTools[i].setFocusable(false);
			
			toolsGroup.add(btnTools[i]);
		}
		
		lblSize.setHorizontalAlignment(JLabel.CENTER);
		lblSize.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 14));
		lblSize.setForeground(Color.WHITE);
		lblSize.setBackground(new Color(0f, 0f, 0f, 0.4f));
		lblSize.setFocusable(false);
		lblSize.setOpaque(true);
		
		btnPanel.setOpaque(false);
		btnPanel.setLayout(new GridLayout(0, 1, 0, 4));
		btnPanel.setFocusable(false);
		btnPanel.add(btnSend);
		btnPanel.add(btnCopy);
		btnPanel.add(btnSave);
		btnPanel.add(lblSize);
		for(int i = 0; i < btnTools.length; i++) {
			if(btnTools.length > i + 1) {
				btnPanel.add(createHorPanel(btnTools[i++], btnTools[i]));
			} else {
				btnPanel.add(btnTools[i]);
			}
		}
		btnPanel.add(btnColor);
		btnPanel.setSize(150, 300);
		btnPanel.setVisible(false);
		
		this.setLayout(null);
		this.add(btnPanel);
		
		internalFrame.addComponentListener(new ComponentListener() {
			public void componentShown(ComponentEvent e) {
				
			}

			public void componentResized(ComponentEvent e) {
				updateSize();
			}
			
			public void componentMoved(ComponentEvent e) {
				updateSize();
			}
			
			public void componentHidden(ComponentEvent e) {
				
			}
		});
		
		internalFrame.setVisible(true);
		this.add(internalFrame);
	}
	
	private JPanel createHorPanel(JComponent... btns) {
		JPanel pnl = new JPanel(new GridLayout(1, 0, 4, 8));
		pnl.setOpaque(false);
		
		for(JComponent b : btns) {
			pnl.add(b);
		}
		
		return pnl;
	}
	
	private void updateSize(){
		Rectangle bounds = this.internalFrame.getArea();
		
		if(bounds.width != 0 && bounds.height != 0) {
			int paneX = Math.max(bounds.x + bounds.width + 4, 0);
			int paneY = Math.max(bounds.y, 0);
			
			if(paneX + btnPanel.getWidth() > this.getWidth()) {
				paneX = this.getWidth() - btnPanel.getWidth();
			}
			
			if(paneY + btnPanel.getHeight() > this.getHeight()) {
				paneY = this.getHeight() - btnPanel.getHeight();
			}
			
			btnPanel.setLocation(paneX, paneY);
			btnPanel.setVisible(true);
		} else {
			btnPanel.setVisible(false);
		}
		
		lblSize.setText(bounds.width + "x" + bounds.height);
	}
	
	public void setArea(int ax, int ay, int bx, int by) {
		this.internalFrame.setBounds(Utils.constructRectangle(ax, ay, bx, by, new Rectangle()));
		this.repaint();
	}
	
	protected void paintComponent(Graphics g) {
		if(this.screen == null){
			return;
		}
		
		Graphics2D g2d = (Graphics2D) g.create();

		g2d.drawImage(screen, 0, 0, this.getWidth(), this.getHeight(), null);
		
		g2d.setColor(OFF_COLOR);
		g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		g2d.dispose();
	}

	public BufferedImage getScreen() {
		return screen;
	}

	public void setScreen(BufferedImage screen) {
		this.screen = screen;
		this.internalFrame.setScreen(screen);
	}

	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		
		if(source == btnSend) {
			BufferedImage subimg = this.internalFrame.getFinalScreen();
			onfinished();
			
			final JProgressDialog progDial = new JProgressDialog();
			progDial.setLocationRelativeTo(null);
			progDial.setAlwaysOnTop(true);
			progDial.setVisible(true);
			
			new Thread(() -> {
				ImgurImage img;
				try {
					img = ImgurAPI.uploadImage(subimg, (p) -> {
						progDial.setPercent((int) (p * 100));
					}, () -> {
						progDial.setLoading();
					});
					
					progDial.showLink(img.getLink());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}).start();
		} else if(source == btnCopy) {
			BufferedImage subimg = this.internalFrame.getFinalScreen();
			Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
			cb.setContents(new ImageTransferable(subimg), null);

			onfinished();
		} else if(source == btnSave) {
			BufferedImage subimg = this.internalFrame.getFinalScreen();
			onfinished();
			fileChooser.showSaveDialog(null);
			
			File selected = fileChooser.getSelectedFile();
			
			if(selected == null) {
				return;
			}
			
			Object result = JOptionPane.showInputDialog(null,
					"Choose the format",
					"Format",
					JOptionPane.PLAIN_MESSAGE,
					null,
					this.formatsImage,
					this.formatsImage[0]); // PNG default
			
			if(result != null) {
				String ioformat = (String) result;
				
				if(ioformat.equals(this.formatsImage[1])) { // "JPG/JPEG"
					ioformat = "jpeg";
				}
				
				try {
					ImageIO.write(subimg, ioformat.toLowerCase(), selected);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		} else if(source == btnColor) {
			Color clr = JColorChooser.showDialog(this, "Choose a color", this.internalFrame.getToolColor());
			
			if(clr != null) {
				this.internalFrame.setToolColor(clr);
				btnColor.setBackground(clr);
			}
		}else if(source instanceof JToggleButton) {
			for(int i = 0; i < TOOLS_VALUES.length; i++){
				JToggleButton toolBtn = btnTools[i];
				
				if(source == toolBtn) {
					if("Unselect".equals(toolBtn.getActionCommand())) {
						toolBtn.setActionCommand("Select");
						toolsGroup.clearSelection();
						this.internalFrame.setTool(Tool.NONE);
					} else { // "Select".equals(toolBtn.getActionCommand())
						toolBtn.setActionCommand("Unselect");
						this.internalFrame.setTool(TOOLS_VALUES[i]);
					}
				} else {
					toolBtn.setActionCommand("Select");
				}
			}
		}
	}
	
	public void onfinished(){
		if(this.onfinished != null)
			this.onfinished.run();
	}
	
	public Runnable getOnFinished() {
		return onfinished;
	}

	public void setOnFinished(Runnable onfinished) {
		this.onfinished = onfinished;
	}

	private static class ImageTransferable implements Transferable {
		private Image image;
		
		public ImageTransferable(Image image) {
		    this.image = image;
		}
		
		public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {
			if (isDataFlavorSupported(flavor)) {
			    return image;
			} else {
			    throw new UnsupportedFlavorException(flavor);
			}
		}
		
		public boolean isDataFlavorSupported(DataFlavor flavor) {
		    return flavor == DataFlavor.imageFlavor;
		}
		
		public DataFlavor[] getTransferDataFlavors() {
			return new DataFlavor[] { DataFlavor.imageFlavor };
		}
	}

	public Rectangle getArea() {
		return internalFrame.getArea();
	}
}
