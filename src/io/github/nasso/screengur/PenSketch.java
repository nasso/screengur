package io.github.nasso.screengur;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.QuadCurve2D;
import java.util.ArrayList;
import java.util.List;

public class PenSketch extends StrokedSketch {
	private List<Point> points = new ArrayList<Point>();
	private QuadCurve2D curve = new QuadCurve2D.Float();
	
	private boolean cacheDirty = true;
	
	public PenSketch(float toolSize, Color toolColor) {
		super(toolSize, toolColor);
	}

	public void addPoint(Point p) {
		this.points.add(p);
		
		cacheDirty = true;
	}
	
	private void updateCache() {
		
		cacheDirty = false;
	}
	
	public void draw(Graphics2D g2d) {
		g2d.setStroke(this.stroke);
		g2d.setColor(this.color);
		
		if(cacheDirty) updateCache();

		float x1 = points.get(0).x;
		float y1 = points.get(0).y;
		float cx = 0;
		float cy = 0;
		float x2 = 0;
		float y2 = 0;
		
		if(points.size() <= 2) {
			if(points.size() == 1) {
				int halfSize = (int) (this.toolSize / 2.0f);
				
				g2d.fillOval(points.get(0).x - halfSize, points.get(0).y - halfSize, halfSize * 2, halfSize * 2);
			} else {
				g2d.drawLine(points.get(0).x, points.get(0).y, points.get(1).x, points.get(1).y);
			}
		} else {
			int i = 1, l = points.size() - 2;
			for(; i < l; i++) {
				cx = points.get(i).x;
				cy = points.get(i).y;
				
				x2 = (points.get(i).x + points.get(i+1).x) / 2.0f;
				y2 = (points.get(i).y + points.get(i+1).y) / 2.0f;
				
				curve.setCurve(x1, y1, cx, cy, x2, y2);
				g2d.draw(curve);
				
				x1 = x2;
				y1 = y2;
			}
			
			curve.setCurve(
					x1,
					y1,
					points.get(i).x,
					points.get(i).y,
					points.get(i + 1).x,
					points.get(i + 1).y);
			g2d.draw(curve);
		}
	}
	
	public Sketch clone() {
		PenSketch sk = new PenSketch(toolSize, color);
		
		for(int i = 0; i < points.size(); i++) {
			sk.addPoint(points.get(i));
		}
		
		return sk;
	}
}
