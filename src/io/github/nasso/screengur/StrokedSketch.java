package io.github.nasso.screengur;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Stroke;

public abstract class StrokedSketch extends Sketch {
	protected Stroke stroke;
	
	public StrokedSketch(float toolSize, Color toolColor) {
		super(toolSize, toolColor);
	}
	
	public void setToolSize(float toolSize) {
		super.setToolSize(toolSize);
		this.stroke = new BasicStroke(toolSize, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	}
}
