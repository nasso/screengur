package io.github.nasso.screengur;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class ScreenAreaInternalFrame extends JPanel implements MouseListener, MouseMotionListener, MouseWheelListener, KeyEventDispatcher {
	private static final long serialVersionUID = 1L;
	
	private static final Color BORDER_COLOR = new Color(1f, 1f, 1f);
	private static final Color BRUSH_COLOR = new Color(0.5f, 0.5f, 0.5f);
	private static final Color TRANSPARENT_COLOR = new Color(0f, 0f, 0f, 0f);
	private static final int RESIZE_DISTANCE = 8;
	
	private static final int RESIZE_NONE = 0b0000;
	private static final int RESIZE_TOP = 0b1000;
	private static final int RESIZE_BOTTOM = 0b0100;
	private static final int RESIZE_RIGHT = 0b0010;
	private static final int RESIZE_LEFT = 0b0001;
	
	private Tool tool = Tool.NONE;
	private float toolSize = 4;
	private Color toolColor = Color.GREEN;
	
	private Graphics2D s2d;
	private Graphics2D f2d;
	private BufferedImage sketches;
	private BufferedImage sketchesFixed;
	private BufferedImage screen;
	private int currentCursor = -1;
	private int resizeMode = RESIZE_NONE;
	
	public static final int MAX_SKETCH_STACK_SIZE = 10;
	private int sketchStackPos = 0;
	private Sketch[] sketchStack = new Sketch[MAX_SKETCH_STACK_SIZE];
	
	private Point pressPoint = new Point();
	private Point dragPoint = new Point();
	private Point mousePos = new Point();
	private boolean isMouseLeftDown = false;
	private boolean isMouseRightDown = false;
	
	private RectangleSketch rectSketch;
	private ArrowSketch arrowSketch;
	private LineSketch lineSketch;
	private PenSketch penSketch;
	
	public ScreenAreaInternalFrame() {
		this.setOpaque(false);
		
		this.addMouseListener(this);
		this.addMouseWheelListener(this);
		this.addMouseMotionListener(this);
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
	}
	
	protected void paintComponent(Graphics g) {
		if(screen == null) {
			return;
		}
		
		fillFrontSketches();
		
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
		Rectangle bounds = this.getBounds();
		g2d.drawImage(screen,
				0, 0, bounds.width, bounds.height,
				bounds.x, bounds.y, bounds.x + bounds.width, bounds.y + bounds.height, null);
		g2d.drawImage(sketches,
				0, 0, bounds.width, bounds.height,
				bounds.x, bounds.y, bounds.x + bounds.width, bounds.y + bounds.height, null);
		
		if(tool != Tool.NONE) {
			g2d.setColor(BRUSH_COLOR);
			g2d.drawOval(
					mousePos.x - this.getX() - (int) (toolSize/2.0f),
					mousePos.y - this.getY() - (int) (toolSize/2.0f),
					(int) toolSize,
					(int) toolSize);
		}
		
		g2d.setColor(BORDER_COLOR);
		g2d.drawRect(0, 0, bounds.width - 1, bounds.height - 1);
		
		g2d.dispose();
	}
	
	private void doSketch(Sketch s) {
		sketchStack[sketchStackPos++] = s;
		
		while(sketchStackPos >= MAX_SKETCH_STACK_SIZE) {
			sketchStack[0].draw(f2d);
			
			for(int i = 0; i < MAX_SKETCH_STACK_SIZE; i++) {
				if(i == MAX_SKETCH_STACK_SIZE-1) sketchStack[i] = null;
				else sketchStack[i] = sketchStack[i+1];
			}
			
			sketchStackPos--;
		}
		
		for(int i = sketchStackPos + 1; i < MAX_SKETCH_STACK_SIZE; i++) {
			sketchStack[i] = null;
		}
		
		this.repaint();
	}
	
	public void actionRedo() {
		if(sketchStackPos < MAX_SKETCH_STACK_SIZE && sketchStack[sketchStackPos] != null) {
			sketchStackPos++;
		}
		
		this.repaint();
	}
	
	public void actionUndo() {
		if(sketchStackPos > 0) {
			sketchStackPos--;
		}
		
		this.repaint();
	}
	
	public BufferedImage getScreen() {
		return screen;
	}
	
	public void setScreen(BufferedImage screen) {
		for(int i = 0; i < sketchStack.length; i++) {
			sketchStack[i] = null;
		}
		sketchStackPos = 0;
		
		this.screen = screen;
		
		if(screen == null) {
			this.sketches = null;
			this.sketchesFixed = null;
			this.s2d = null;
		} else {
			if(s2d != null) {
				s2d.dispose();
				s2d = null;
			}
			
			if(f2d != null) {
				f2d.dispose();
				f2d = null;
			}
			
			this.sketches = new BufferedImage(screen.getWidth(), screen.getHeight(), BufferedImage.TYPE_INT_ARGB);
			this.sketchesFixed = new BufferedImage(screen.getWidth(), screen.getHeight(), BufferedImage.TYPE_INT_ARGB);

			s2d = sketches.createGraphics();
			f2d = sketchesFixed.createGraphics();
			
			s2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			s2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			s2d.setBackground(TRANSPARENT_COLOR);
			s2d.setComposite(AlphaComposite.Src);
			
			f2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			f2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			f2d.setBackground(TRANSPARENT_COLOR);
			f2d.setComposite(AlphaComposite.Src);
			
			this.setToolColor(toolColor);
			this.setToolSize(toolSize);
		}
	}
	
	private void fillFrontSketches() {
		Rectangle b = getArea();
		
		s2d.clearRect(b.x, b.y, b.width, b.height);
		s2d.drawImage(sketchesFixed,
				b.x, b.y, b.x + b.width, b.y + b.height,
				b.x, b.y, b.x + b.width, b.y + b.height, null);
		
		for(int i = 0; i < sketchStackPos; i++) {
			if(sketchStack[i] instanceof TextSketch) {
				TextSketch txtsk = (TextSketch) sketchStack[i];

				txtsk.setEditMode(i == sketchStackPos - 1);
			}
			
			sketchStack[i].draw(s2d);
		}
		
		if(lineSketch != null) lineSketch.draw(s2d);
		if(penSketch != null) penSketch.draw(s2d);
		if(arrowSketch != null) arrowSketch.draw(s2d);
		if(rectSketch != null) rectSketch.draw(s2d);
	}
	
	public BufferedImage getFinalScreen() {
		Rectangle b = getArea();
		
		fillFrontSketches();
		
		BufferedImage f = new BufferedImage(b.width, b.height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = f.createGraphics();
		g2d.drawImage(screen, 0, 0, b.width, b.height, b.x, b.y, b.x + b.width, b.y + b.height, null);
		g2d.drawImage(sketchesFixed, 0, 0, b.width, b.height, b.x, b.y, b.x + b.width, b.y + b.height, null);
		g2d.drawImage(sketches, 0, 0, b.width, b.height, b.x, b.y, b.x + b.width, b.y + b.height, null);
		
		g2d.dispose();
		
		return f;
	}
	
	public Rectangle getArea() {
		Rectangle screenArea = this.getBounds();
		
		if(this.screen != null) {
			int maxX = screenArea.x + screenArea.width;
			int maxY = screenArea.y + screenArea.height;
			
			screenArea.x = Math.min(Math.max(0, screenArea.x), screen.getWidth());
			screenArea.y = Math.min(Math.max(0, screenArea.y), screen.getHeight());
			
			screenArea.width = maxX - screenArea.x;
			screenArea.height = maxY - screenArea.y;
			
			if(screenArea.x + screenArea.width > screen.getWidth()) {
				screenArea.width = screen.getWidth() - screenArea.x;
			}
			
			if(screenArea.y + screenArea.height > screen.getHeight()) {
				screenArea.height = screen.getHeight() - screenArea.y;
			}
		}
		
		return screenArea;
	}
	
	public void mouseDragged(MouseEvent e) {
		mousePos.setLocation(e.getLocationOnScreen());
		
		if(isMouseLeftDown) { // -> BUTTON1
			if(tool == Tool.NONE) {
				dragPoint = e.getLocationOnScreen();
				
				if(this.resizeMode == RESIZE_NONE) {
					this.setLocation(dragPoint.x - pressPoint.x + this.getX(), dragPoint.y - pressPoint.y + this.getY());
				} else {
					int diffx = dragPoint.x - pressPoint.x;
					int diffy = dragPoint.y - pressPoint.y;
					
					if((this.resizeMode & RESIZE_TOP) != 0) {
						int maxY = this.getY() + this.getHeight();
						this.setBounds(this.getX(), this.getY() + diffy, this.getWidth(), this.getHeight() - diffy);
						
						this.setSize(this.getWidth(), Math.max(1, this.getHeight()));
						this.setLocation(this.getX(), Math.min(maxY - 1, this.getY()));
					} else if((this.resizeMode & RESIZE_BOTTOM) != 0) {
						this.setBounds(this.getX(), this.getY(), this.getWidth(), this.getHeight() + diffy);
						
						this.setSize(this.getWidth(), Math.max(1, this.getHeight()));
					}
					
					if((this.resizeMode & RESIZE_LEFT) != 0) {
						int maxX = this.getX() + this.getWidth();
						this.setBounds(this.getX() + diffx, this.getY(), this.getWidth() - diffx, this.getHeight());
						
						this.setSize(Math.max(1, this.getWidth()), this.getHeight());
						this.setLocation(Math.min(maxX - 1, this.getX()), this.getY());
					} else if((this.resizeMode & RESIZE_RIGHT) != 0) {
						this.setBounds(this.getX(), this.getY(), this.getWidth() + diffx, this.getHeight());
						
						this.setSize(Math.max(1, this.getWidth()), this.getHeight());
					}
				}
				
				pressPoint.setLocation(dragPoint);
			} else if(tool == Tool.PEN) {
				dragPoint = e.getLocationOnScreen();

				arrowSketch = null;
				rectSketch = null;
				lineSketch = null;
				if(penSketch == null) {
					penSketch = new PenSketch(toolSize, toolColor);
				}
				
				penSketch.addPoint(dragPoint);
				
				this.repaint();
			} else if(tool == Tool.LINE) {
				dragPoint = e.getLocationOnScreen();
				
				arrowSketch = null;
				rectSketch = null;
				penSketch = null;
				if(lineSketch == null) {
					lineSketch = new LineSketch(0, 0, 0, 0, toolSize, toolColor);
				}
				
				lineSketch.setValues(pressPoint.x, pressPoint.y, dragPoint.x, dragPoint.y);
				
				this.repaint();
			} else if(tool == Tool.ARROW) {
				dragPoint = e.getLocationOnScreen();
				
				lineSketch = null;
				rectSketch = null;
				penSketch = null;
				if(arrowSketch == null) {
					arrowSketch = new ArrowSketch(0, 0, 0, 0, toolSize, toolColor);
				}
				
				arrowSketch.setValues(pressPoint.x, pressPoint.y, dragPoint.x, dragPoint.y);
				
				this.repaint();
			} else if(tool == Tool.RECTANGLE) {
				dragPoint = e.getLocationOnScreen();
				
				arrowSketch = null;
				lineSketch = null;
				penSketch = null;
				if(rectSketch == null) {
					rectSketch = new RectangleSketch(0, 0, 0, 0, toolSize, toolColor);
				}
				
				rectSketch.setValues(pressPoint.x, pressPoint.y, dragPoint.x, dragPoint.y);
				
				this.repaint();
			}
		} else if(isMouseRightDown) {
			if(tool != Tool.NONE) {
				dragPoint = e.getLocationOnScreen();

				arrowSketch = null;
				rectSketch = null;
				lineSketch = null;
				if(penSketch == null) {
					penSketch = new PenSketch(toolSize, TRANSPARENT_COLOR);
				}
				
				penSketch.addPoint(dragPoint);
				
				this.repaint();
			}
		}
	}

	public void mouseMoved(MouseEvent e) {
		Rectangle bounds = this.getBounds();
		mousePos.setLocation(e.getLocationOnScreen());
		
		this.resizeMode = RESIZE_NONE;
		if(Math.abs(mousePos.x - bounds.x) < RESIZE_DISTANCE) {
			this.resizeMode |= RESIZE_LEFT;
		}
		
		if(Math.abs(mousePos.x - bounds.x - bounds.width) < RESIZE_DISTANCE) {
			this.resizeMode |= RESIZE_RIGHT;
		}
		
		if(Math.abs(mousePos.y - bounds.y) < RESIZE_DISTANCE) {
			this.resizeMode |= RESIZE_TOP;
		}
		
		if(Math.abs(mousePos.y - bounds.y - bounds.height) < RESIZE_DISTANCE) {
			this.resizeMode |= RESIZE_BOTTOM;
		}
		
		int cursor = Cursor.DEFAULT_CURSOR;
		if(tool == Tool.NONE) {
			cursor = Cursor.MOVE_CURSOR;
			if(
					(this.resizeMode & RESIZE_TOP) != 0 && (this.resizeMode & RESIZE_RIGHT) != 0 ||
					(this.resizeMode & RESIZE_BOTTOM) != 0 && (this.resizeMode & RESIZE_LEFT) != 0) {
				cursor = Cursor.NE_RESIZE_CURSOR;
			} else if(
					(this.resizeMode & RESIZE_TOP) != 0 && (this.resizeMode & RESIZE_LEFT) != 0 ||
					(this.resizeMode & RESIZE_BOTTOM) != 0 && (this.resizeMode & RESIZE_RIGHT) != 0) {
				cursor = Cursor.NW_RESIZE_CURSOR;
			} else if((this.resizeMode & RESIZE_TOP) != 0 || (this.resizeMode & RESIZE_BOTTOM) != 0) {
				cursor = Cursor.N_RESIZE_CURSOR;
			} else if((this.resizeMode & RESIZE_LEFT) != 0 || (this.resizeMode & RESIZE_RIGHT) != 0) {
				cursor = Cursor.E_RESIZE_CURSOR;
			}
		}
		
		if(cursor != -1 && this.currentCursor != cursor) {
			this.setCursor(Cursor.getPredefinedCursor(cursor));
			this.currentCursor = cursor;
		}
		
		this.repaint();
	}

	public void mouseClicked(MouseEvent e) {
		
	}

	public void mousePressed(MouseEvent e) {
		if(e.getButton() == 1) {
			pressPoint.setLocation(e.getLocationOnScreen());
			
			if(tool == Tool.TEXT) {
				arrowSketch = null;
				rectSketch = null;
				lineSketch = null;
				penSketch = null;
				
				doSketch(new TextSketch(pressPoint.x, pressPoint.y, toolSize, toolColor));
				
				this.repaint();
			}
			
			isMouseLeftDown = true;
		} else if(e.getButton() == 3) {
			pressPoint.setLocation(e.getLocationOnScreen());
			
			isMouseRightDown = true;
		}
		
		// Trick
		this.mouseDragged(e);
	}

	public void mouseReleased(MouseEvent e) {
		if(e.getButton() == MouseEvent.BUTTON1) {
			if(penSketch != null) {
				doSketch(penSketch.clone());
			}
			
			if(lineSketch != null) {
				doSketch(lineSketch.clone());
			}
			
			if(arrowSketch != null) {
				doSketch(arrowSketch.clone());
			}
			
			if(rectSketch != null) {
				doSketch(rectSketch.clone());
			}
			
			penSketch = null;
			lineSketch = null;
			arrowSketch = null;
			rectSketch = null;
			
			isMouseLeftDown = false;
			
			this.repaint();
		} else if(e.getButton() == 3) {
			if(penSketch != null) {
				doSketch(penSketch.clone());
			}

			penSketch = null;
			isMouseRightDown = false;
		}
	}

	public void mouseEntered(MouseEvent e) {
		
	}

	public void mouseExited(MouseEvent e) {
		
	}

	public float getToolSize() {
		return toolSize;
	}

	public void setToolSize(float toolSize) {
		this.toolSize = Math.max(toolSize, 1);
		
		if(sketchStackPos != 0 && sketchStack[sketchStackPos-1] != null) {
			sketchStack[sketchStackPos-1].setToolSize(toolSize);
		}
		
		this.repaint();
	}

	public Color getToolColor() {
		return toolColor;
	}

	public void setToolColor(Color toolColor) {
		this.toolColor = toolColor;
	}

	public Tool getTool() {
		return tool;
	}

	public void setTool(Tool tool) {
		this.tool = tool;
	}

	public void mouseWheelMoved(MouseWheelEvent e) {
		this.setToolSize(this.toolSize += e.getWheelRotation());
	}

	public boolean dispatchKeyEvent(KeyEvent e) {
		if(sketchStackPos != 0 && sketchStack[sketchStackPos - 1] instanceof TextSketch) {
			TextSketch txtsk = (TextSketch) sketchStack[sketchStackPos - 1];

			if(e.getID() == KeyEvent.KEY_TYPED) {
				txtsk.keyTyped(e);
			} else if(e.getID() == KeyEvent.KEY_PRESSED) {
				txtsk.keyPressed(e);
			} else if(e.getID() == KeyEvent.KEY_RELEASED) {
				txtsk.keyReleased(e);
			}
		}
		
		this.repaint();
		
		return false;
	}
}
