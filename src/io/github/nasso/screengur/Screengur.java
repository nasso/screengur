package io.github.nasso.screengur;

import java.awt.AWTException;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.Rectangle;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

public class Screengur {
	private TrayIcon icon;
	
	private ScreenAreaFrame screenAreaFrame;
	
	public Screengur() {
		
		PopupMenu popup = new PopupMenu();
		MenuItem takeScreenMenuItem = new MenuItem("Take a screenshot");
		MenuItem quitMenuItem = new MenuItem("Quit");
		
		takeScreenMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				takeScreenshot();
			}
		});
		
		quitMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quit();
			}
		});
		
		popup.add(takeScreenMenuItem);
		popup.add(quitMenuItem);
		
		// Gets the total width and height of all screens
		GraphicsDevice[] screens = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
		int width = 0;
		int height = 0;
		for(GraphicsDevice device : screens) {
			Rectangle bounds = device.getDefaultConfiguration().getBounds();
			
			width = Math.max(bounds.x + bounds.width, width);
			height = Math.max(bounds.y + bounds.height, height);
		}
		
		screenAreaFrame = new ScreenAreaFrame(width, height);
		
		try {
			icon = new TrayIcon(RessourceLoader.getImage("res/icon.png").getScaledInstance(16, 16, Image.SCALE_SMOOTH));
			icon.setToolTip("Screengur D\nPress PrintScreen to take a screenshot");
			
			icon.setPopupMenu(popup);
			
			try {
				SystemTray.getSystemTray().add(icon);
			} catch (AWTException e1) {
				e1.printStackTrace();
			}
		} catch(Exception e) {
			System.out.println("Something bad happened when trying to use the TrayIcon feature...");
			System.out.println("No worries thought, you won't have an icon in the taskbar, that's all!");
		}
	}
	
	private void quit() {
		screenAreaFrame.dispose();
		System.exit(0);
	}
	
	private void takeScreenshot() {
		screenAreaFrame.takeScreenshot();
	}
	
	public static void main(String[] argv) {
		SwingUtilities.invokeLater(() -> {
			Screengur screenTaker = new Screengur();
			
			try {
				// Disable logging
				Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
				logger.setLevel(Level.OFF);
				logger.setUseParentHandlers(false);
				
				GlobalScreen.registerNativeHook();
				GlobalScreen.addNativeKeyListener(new NativeKeyListener() {
					public void nativeKeyTyped(NativeKeyEvent e) {
						
					}
					
					public void nativeKeyReleased(NativeKeyEvent e) {
						
					}
					
					public void nativeKeyPressed(NativeKeyEvent e) {
						if(NativeKeyEvent.VC_PRINTSCREEN == e.getKeyCode()) {
							screenTaker.takeScreenshot();
						}
					}
				});
			} catch (NativeHookException e1) {
				e1.printStackTrace();
			}
		});
	}
}
