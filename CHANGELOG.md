# Changelog

## 1.2
- Added: Tool size
- Added: Text tool
- Added: Erase tool
- Added: Undo/Redo
- Added: Changing the tool size affects the last drawn shape
- Added: Lines drawn with the pen tool are smoother
- Fixed: Transparent colors are better used
- Fixed: Draw as soon as the mouse is pressed (not dragged)

## 1.1
- Added: Drawing tools (pen, line, arrow, rectangle)

## 1.0
- Added: Send screenshots to imgur
- Added: Copy the image to the clipboard
- Added: Save the image to a file (supported formats: PNG, JPG/JPEG, BMP)
- Added: Support multi-monitors
